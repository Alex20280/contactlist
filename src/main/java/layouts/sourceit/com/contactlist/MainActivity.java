package layouts.sourceit.com.contactlist;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    EditText name;
    EditText email;
    ImageView image;
    ListView list;
    LinearLayout itemMain;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        name = (EditText) findViewById(R.id.name);
        email = (EditText) findViewById(R.id.email);
        image = (ImageView) findViewById(R.id.image);
        list = (ListView) findViewById(R.id.list);
        itemMain = (LinearLayout) findViewById(R.id.itemMain);

        ArrayList<String> namesList = new ArrayList<>();
        namesList.add("Alex");
        namesList.add("Paul");
        namesList.add("John");
        namesList.add("Dave");
        namesList.add("Alex");
        namesList.add("Paul");
        namesList.add("John");
        namesList.add("Dave");

        ArrayList<String> emailList = new ArrayList<>();
        emailList.add("alex@gmail.com");
        emailList.add("paul@gmail.com");
        emailList.add("john@gmail.com");
        emailList.add("dave@gmail.com");
        emailList.add("alex@gmail.com");
        emailList.add("john@gmail.com");
        emailList.add("dave@gmail.com");
        emailList.add("alex@gmail.com");

        ArrayList<Integer> imageList = new ArrayList<>();
        imageList.add(R.drawable.contact1);
        imageList.add(R.drawable.contact2);
        imageList.add(R.drawable.contact3);
        imageList.add(R.drawable.contact4);
        imageList.add(R.drawable.contact5);
        imageList.add(R.drawable.contact6);
        imageList.add(R.drawable.contact7);
        imageList.add(R.drawable.contact8);

        MySimpleArrayAdapter mySimpleArrayAdapter = new MySimpleArrayAdapter(this, namesList);
        list.setAdapter(mySimpleArrayAdapter);

        itemMain.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, SecondActivity.class);
                intent.putExtra("name", name.getText().toString());
                intent.putExtra("email", email.getText().toString());
                intent.putExtra("image", R.drawable.contact1);//???????????
                startActivity(intent);
            }
        });

    }


    public class MySimpleArrayAdapter extends ArrayAdapter<String> {
        public MySimpleArrayAdapter(Context context, ArrayList<String> values) {
            super(context, R.layout.item, values);
        }

        @NonNull
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            View rowView = convertView;
            if (rowView == null) {
                rowView = getLayoutInflater().inflate(R.layout.item, null, true);
                holder = new ViewHolder();
                holder.name = (EditText) rowView.findViewById(R.id.name);
                holder.email = (EditText) rowView.findViewById(R.id.email);
                holder.image = (ImageView) rowView.findViewById(R.id.image);
                rowView.setTag(holder);
            } else {
                holder = (ViewHolder) rowView.getTag();
            }
            holder.name.setText(getItem(position));
            holder.email.setText(getItem(position));
            holder.image.setImageResource(position);
            return rowView;
        }
    }

    static class ViewHolder {
        public EditText name;
        public EditText email;
        public ImageView image;
    }

}

