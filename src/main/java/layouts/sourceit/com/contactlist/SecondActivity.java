package layouts.sourceit.com.contactlist;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class SecondActivity extends AppCompatActivity {

    TextView detailedName;
    TextView detailedEmail;
    TextView detailedPhone;
    TextView detailedAddress;
    ImageView detailedImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        detailedName = (TextView) findViewById(R.id.detailedName);
        detailedEmail = (TextView) findViewById(R.id.detailedEmail);
        detailedPhone = (TextView) findViewById(R.id.detailedPhone);
        detailedAddress = (TextView) findViewById(R.id.detailedAddress);
        detailedImage = (ImageView) findViewById(R.id.detailedImage);

        Intent intent = getIntent();
        String name = getIntent().getExtras().getString("name");
        String email = getIntent().getExtras().getString("email");
        int image = getIntent().getExtras().getInt("image");

        detailedName.setText(name);
        detailedEmail.setText(email);
        detailedImage.setImageResource(image);
    }
}
